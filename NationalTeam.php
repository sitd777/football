<?php

class NationalTeam {

    /**
     * @var int
     * Holds team ID
     */
    public $id = 0;

    /**
     * @var string
     * Holds country name
     */
    public $name = '';

    /**
     * @var int
     * Holds count of total matches
     */
    public $matches_total = 0;

    /**
     * @var int
     * Holds count of winned matches
     */
    public $matches_win = 0;

    /**
     * @var int
     * Holds count of dead heat matches
     */
    public $matches_deadheat = 0;

    /**
     * @var int
     * Holds count of failed matches
     */
    public $matches_fail = 0;

    /**
     * @var int
     * Holds count of scored goals
     */
    public $goals_scored = 0;

    /**
     * @var int
     * Holds count of missed goals
     */
    public $goals_missed = 0;

    /**
     * @var float
     * Holds coefficient
     */
    public $k = 0.0;

    /**
     * Country constructor.
     */
    public function __construct($id, $data) {
        $this->id               = $id;
        $this->name             = $data[0];
        $this->matches_total    = (int)$data[1];
        $this->matches_win      = (int)$data[2];
        $this->matches_deadheat = (int)$data[3];
        $this->matches_fail     = (int)$data[4];
        $goals = explode('-', $data[5]);
        $this->goals_scored     = (int)trim($goals[0]);
        $this->goals_missed     = (int)trim($goals[1]);
        $this->k = number_format($this->matches_win / $this->matches_total, 1, '.', '');
    }

    /**
     * @var string
     */
    const PATH_DATA = __DIR__ . '/data.csv';

    /**
     * @var array
     *
     * Holds national teams data
     */
    static protected $_list = [];

    /**
     * @throws Exception
     * Loads data from CSV
     */
    static public function loadData() {
        // Check file existing
        if(!file_exists(self::PATH_DATA)) {
            throw new Exception("No data file.");
        }

        // Load data
        $fh = fopen(self::PATH_DATA, 'r');
        self::$_list = [];
        if($fh) {
            $row = 0;
            while(($data = fgetcsv($fh, null, ";")) !== FALSE) {
                if($row > 0) { // Skip headers
                    $country = new NationalTeam($row, $data);
                    self::$_list[] = $country;
                }
                $row++;
            }
            fclose($fh);
        }
    }

    /**
     * @return array
     *
     * Returns list of national teams
     */
    static public function getList() {
        return self::$_list;
    }

    /**
     * @return array
     * Plays match
     */
    static public function play($team1, $team2) {
        $team1k = $team2k = 0;
        while($team1k == $team2k) {
            $team1k = rand($team1->k * 10 - 1, $team1->k * 10 + 1);
            $team2k = rand($team2->k * 10 - 1, $team2->k * 10 + 1);
        }
        $diffK = abs($team1k - $team2k);
        if($diffK < 2) $diffK = 2;
        $scoreL = rand(0, 2);
        $scoreH = $scoreL + rand(1, $diffK);
        if($team1k > $team2k) return [$scoreH, $scoreL];
        return [$scoreL, $scoreH];
    }
}