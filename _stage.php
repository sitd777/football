<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $stageTitle ?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <?php
            shuffle($stage);
            $nextStage = [];
            while(sizeof($stage) >= 2) {
                $team1 = array_shift($stage);
                $team2 = array_shift($stage);
                $result = NationalTeam::play($team1, $team2);
                $nextStage[] = $result[0] > $result[1] ? $team1 : $team2;
                echo '<div class="col-sm-3"><div class="match panel panel-warning"><div class="panel-body">' . $team1->name . ' - ' . $team2->name . '<br/>
                <span class="badge badge-warning">' . $result[0] . ':' . $result[1] . '</span></div></div></div>';
            }
            ?>
        </div>
    </div>
</div>