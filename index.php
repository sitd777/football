<?php

require_once './NationalTeam.php';
NationalTeam::loadData();

$go = !empty($_POST['go']);

?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Football championship</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
</head>
<body>
<div class="wrap">
    <nav id="w0" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Эмулятор футбольного чемпионата</a>
            </div>
        </div>
    </nav>
    <div class="container main">
        <section class="messages">
        </section>

        <section class="content">
            <div class="row actions">
                <form method="post">
                    <button class="btn btn-primary" name="go" value="1">«Запустить чемпионат»</button>
                </form>
            </div>
            <p>Коэффициент комманды = Количество побед / Количество всех игр</p>
            <?php if($go) { ?>
            <div class="row">
                <div class="col-sm-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Команды</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <ul class="list-group">
                                    <?php foreach(NationalTeam::getList() as $country) { ?>
                                    <li class="list-group-item justify-content-between">
                                        <?= $country->name ?>
                                        <span class="badge badge-info"><?= $country->k ?></span>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <?php
                    $stageTitles = [
                        '1 этап',
                        '2 этап',
                        '3 этап',
                        'Полуфинал',
                        'Финал',
                    ];
                    $nextStage = NationalTeam::getList();

                    foreach($stageTitles as $stageTitle) {
                        $stage = $nextStage;
                        require '_stage.php';
                    }
                    ?>
                </div>
            </div>
            <?php } ?>
        </section>
    </div>
</div>
</body>
</html>